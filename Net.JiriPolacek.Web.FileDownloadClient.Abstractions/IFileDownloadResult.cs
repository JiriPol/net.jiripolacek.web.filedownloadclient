﻿using System;
using System.IO;

namespace Net.JiriPolacek.Web.FileDownloadClient.Abstractions
{
    /// <summary>
    /// Download result.
    /// </summary>
    public interface IFileDownloadResult
    {
        /// <summary>
        /// Identification of the result.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Result status.
        /// </summary>
        ResultStatus Status { get; }

        /// <summary>
        /// Result message.
        /// </summary>
        string Message { get; }

        /// <summary>
        /// The local file that is to receive the data.
        /// </summary>
        FileInfo FileInfo { get; }

        /// <summary>
        /// The URI specified as a String, from which to download data. 
        /// </summary>
        Uri Uri { get; }
    }
}