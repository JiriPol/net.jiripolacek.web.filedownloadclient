﻿namespace Net.JiriPolacek.Web.FileDownloadClient.Abstractions
{
    /// <summary>
    /// Status of the result.
    /// </summary>
    public enum ResultStatus
    {
        Ok,
        Timeout,
        Error
    }
}