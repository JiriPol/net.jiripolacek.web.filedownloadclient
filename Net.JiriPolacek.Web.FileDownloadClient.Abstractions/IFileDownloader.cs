﻿using System;

namespace Net.JiriPolacek.Web.FileDownloadClient.Abstractions
{
    public interface IFileDownloader
    {
        /// <summary>
        /// Asynchronously downloads the file.
        /// </summary>
        /// <param name="timeoutInMilisonds">Timeout in miliseconds.</param>
        void DownloadAsync(int timeoutInMilisonds);

        /// <summary>
        /// Subscribes file download result observer.
        /// </summary>
        /// <param name="observer">Download result observer.</param>
        /// <returns>Observer unsubscriber.</returns>
        IDisposable Subscribe(IObserver<IFileDownloadResult> observer);
    }
}