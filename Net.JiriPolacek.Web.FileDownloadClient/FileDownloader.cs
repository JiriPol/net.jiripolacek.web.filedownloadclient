﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using Net.JiriPolacek.Web.FileDownloadClient.Abstractions;

namespace Net.JiriPolacek.Web.FileDownloadClient
{
    public class FileDownloader : IFileDownloader, IObservable<IFileDownloadResult>
    {
        private Thread m_timeoutThread;
        private Thread m_downloadThread;

        private readonly Uri m_uri;
        private readonly FileInfo m_fileInfo;

        private readonly IList<IObserver<IFileDownloadResult>> m_observers 
            = new List<IObserver<IFileDownloadResult>>();

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="uri">
        /// The URI specified as a String, from which to download data. 
        /// </param>
        /// <param name="fileInfo">
        /// The local file that is to receive the data. 
        /// </param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="uri"/> or <paramref name="fileInfo"/> is null reference.</exception>
        /// <exception cref="ArgumentException">Thrown if <paramref name="fileInfo"/> is a directory.</exception>
        public FileDownloader(Uri uri, FileInfo fileInfo)
        {
            if (uri == null)
            {
                throw new ArgumentNullException(nameof(uri));
            }

            if (fileInfo == null)
            {
                throw new ArgumentNullException(nameof(fileInfo));
            }

            FileAttributes attributes = fileInfo.Attributes;
            FileAttributes dir = fileInfo.Attributes & FileAttributes.Directory;
            if ((int)attributes != -1 && dir != 0)
            {
                throw new ArgumentException("Provided file is directory", nameof(fileInfo));
            }

            m_uri = uri;
            m_fileInfo = fileInfo;
        }

        /// <summary>
        /// Asynchronously downloads the file.
        /// </summary>
        /// <param name="timeoutInMilisonds">Download timeout in miliseconds (default 3600)</param>
        public void DownloadAsync(int timeoutInMilisonds = 3600)
        {
            StartTimeout(timeoutInMilisonds);
            StartDownload();
        }

        /// <summary>
        /// Adds subscriber to file download result.
        /// </summary>
        /// <param name="observer">Download result subscriber.</param>
        /// <returns>Observer unsubscriber.</returns>
        public IDisposable Subscribe(IObserver<IFileDownloadResult> observer)
        {
            if (!m_observers.Contains(observer))
            {
                m_observers.Add(observer);
            }

            return new FileDownloaderUnsubscriber(m_observers, observer);
        }

        private void OnNext(IFileDownloadResult fileDownloadResult)
        {
            foreach (IObserver<IFileDownloadResult> observer in m_observers)
            {
                observer.OnNext(fileDownloadResult);
                observer.OnCompleted();
            }
        }

        private void OnError(Exception e)
        {
            foreach (IObserver<IFileDownloadResult> observer in m_observers)
            {
                observer.OnError(e);
            }
        }

        /// <summary>
        /// Starts timeout thread.
        /// If timeout thread times out, the m_download thread is aborted.
        /// </summary>
        /// <param name="timeoutInMiliseconds"></param>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Thrown if <paramref name="timeoutInMiliseconds"/> value is negative and is not equal to Infinite. 
        /// </exception>
        private void StartTimeout(int timeoutInMiliseconds)
        {
            m_timeoutThread = new Thread(new ThreadStart(() =>
            {
                Thread.Sleep(timeoutInMiliseconds);
                m_downloadThread.Abort();

                string message = $"Download timeout ({timeoutInMiliseconds}) miliseconds";
                IFileDownloadResult fileDownloadResult = FileDownloadResult.Factory.CreateResult(ResultStatus.Timeout, message, m_fileInfo, m_uri);
                OnNext(fileDownloadResult);
            }));

            m_timeoutThread.Start();
        }

        private void StartDownload()
        {
            m_downloadThread = new Thread(new ThreadStart(() =>
            {
                using (System.Net.WebClient webClient = new System.Net.WebClient())
                {
                    try
                    {
                        webClient.DownloadFile(m_uri, m_fileInfo.FullName);
                        string message = "File downloaded.";
                        IFileDownloadResult fileDownloadResult = FileDownloadResult.Factory.CreateResult(ResultStatus.Ok, message, m_fileInfo, m_uri);

                        OnNext(fileDownloadResult);
                    }
                    catch (ArgumentNullException argumentNullException)
                    {
                        m_timeoutThread.Abort();

                        OnError(argumentNullException);
                    }
                    catch (WebException webException)
                    {
                        m_timeoutThread.Abort();

                        OnError(webException);
                    }
                    catch (NotSupportedException notSupportedException)
                    {
                        m_timeoutThread.Abort();

                        OnError(notSupportedException);
                    }
                }
            }));

            m_downloadThread.Start();
        }
    }
}