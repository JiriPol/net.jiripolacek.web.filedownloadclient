﻿using System;
using System.IO;
using Net.JiriPolacek.Web.FileDownloadClient.Abstractions;

namespace Net.JiriPolacek.Web.FileDownloadClient
{
    public sealed class FileDownloadResult : IFileDownloadResult
    {
        private readonly Uri m_uri;

        public Guid Id { get; }
        public ResultStatus Status { get; }
        public string Message { get; }
        public FileInfo FileInfo { get; }
        public Uri Uri => new Uri(m_uri.OriginalString);

        private FileDownloadResult(ResultStatus status, string message, FileInfo tempFileInfo, Uri uri)
        {
            Id = Guid.NewGuid();
            Status = status;
            Message = message;
            FileInfo = tempFileInfo;
            m_uri = uri;
        }

        /// <summary>
        /// FileDownloadResult factory.
        /// </summary>
        public static class Factory
        {
            /// <summary>
            /// Creates instance of IFileDownloadResult.
            /// </summary>
            /// <param name="status">Status of result.</param>
            /// <param name="message">Result message.</param>
            /// <param name="fileInfo">The local file that is to receive the data.</param>
            /// <param name="uri">The URI specified as a String, from which to download data. </param>
            /// <returns>Instance of IFileDownloadResult</returns>
            /// <exception cref="ArgumentNullException">
            /// Thrown if 
            /// <paramref name="fileInfo"/> or 
            /// <paramref name="uri"/> or
            /// <paramref name="message"/> is null reference.
            /// </exception>
            public static IFileDownloadResult CreateResult(ResultStatus status, string message, FileInfo fileInfo, Uri uri)
            {
                if (message == null)
                {
                    throw new ArgumentNullException(nameof(message));
                }
                if (fileInfo == null)
                {
                    throw new ArgumentNullException(nameof(fileInfo));
                }

                if (uri == null)
                {
                    throw new ArgumentNullException(nameof(uri));
                }

                return new FileDownloadResult(status, message, fileInfo, uri);
            }
        }
    }
}