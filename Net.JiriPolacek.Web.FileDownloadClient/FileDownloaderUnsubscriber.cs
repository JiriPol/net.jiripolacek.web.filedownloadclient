﻿using System;
using System.Collections.Generic;
using Net.JiriPolacek.Web.FileDownloadClient.Abstractions;

namespace Net.JiriPolacek.Web.FileDownloadClient
{
    public class FileDownloaderUnsubscriber : IDisposable
    {
        private readonly IList<IObserver<IFileDownloadResult>> m_observers;
        private readonly IObserver<IFileDownloadResult> m_observer;

        /// <summary>
        /// Contructor.
        /// </summary>
        /// <param name="observers">List of observers.</param>
        /// <param name="observer">Observer to remove from the list.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="observers"/> or <paramref name="observer"/> is null reference.</exception>
        public FileDownloaderUnsubscriber(IList<IObserver<IFileDownloadResult>> observers, IObserver<IFileDownloadResult> observer)
        {
            if (observers == null)
            {
                throw new ArgumentNullException(nameof(observers));
            }

            if (observer == null)
            {
                throw new ArgumentNullException(nameof(observer));
            }

            m_observers = observers;
            m_observer = observer;
        }

        /// <summary>
        /// Unsubscibes download result observer.
        /// </summary>
        public void Dispose()
        {
            if (m_observers != null && m_observer != null)
            {
                m_observers.Remove(m_observer);
            }
        }
    }
}